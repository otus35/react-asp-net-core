import { useEffect, useState } from 'react';
import './App.css';

type FetchData = {
  date: Date;
  temperatureC: number;
  temperatureF: number;
  summary: string
}

const checkResponce = async (response: Response) => {
  const data = response.json();
  if (!response.ok) {
    const result = await data.then(result => result);
    if (result.message) {
      throw new Error(result.message);
    }
    else {
      throw new Error("Запрос вернул status = " + response.status);
    }
  }
  else {
    return data;
  }
}

function App() {
  const [data, setData] = useState<Array<FetchData>>();
  const [fetchResult, setFetchResult] = useState("");
  useEffect(() => {

    fetch('https://localhost:44349/WeatherForecast', { method: 'post' })
      .then(checkResponce)
      .then((data: any) => {
        setFetchResult("Запрос успешно выполнен");
        setData(data)
      })
      .catch(ex => {
        setFetchResult("Запрос выполнился с ошибкой: " + ex);
        console.error(ex);
      })
  }, []);

  return (
    <div className="App">
      <header className="App-header">
        <p>
          {fetchResult}
        </p>
        {
          data &&
          <table>
            <thead>
              <tr>
                <th>Время</th>
                <th>Температура С</th>
                <th>Описание</th>
              </tr>
            </thead>
            <tbody>
              {data.map(x => <tr key={x.date.toString()}>
                <td>{x.date.toString()}</td>
                <td>{x.temperatureC}</td>
                <td>{x.summary}</td>
              </tr>)}
            </tbody>
          </table>
        }

      </header>
    </div>
  );
}

export default App;
